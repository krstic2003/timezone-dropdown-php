<?php
if (!empty($_POST) && $_POST['timezone'] != 'default') {
	date_default_timezone_set(htmlspecialchars($_POST['timezone']));
}else{
	date_default_timezone_set('Europe/Belgrade');
}

echo date_default_timezone_get();
echo '<br>';
echo date("Y-m-d H:i:s");

function timezone_list() {
    static $timezones = null;

    if ($timezones === null) {
        $timezones = [];
        $offsets = [];
        $now = new DateTime('now', new DateTimeZone('UTC'));

        foreach (DateTimeZone::listIdentifiers() as $timezone) {
            $now->setTimezone(new DateTimeZone($timezone));
            $offsets[] = $offset = $now->getOffset();
            $timezones[$timezone] = '(' . format_GMT_offset($offset) . ') ' . format_timezone_name($timezone);
        }

        array_multisort($offsets, $timezones);
    }

    return $timezones;
}

function format_GMT_offset($offset) {
    $hours = intval($offset / 3600);
    $minutes = abs(intval($offset % 3600 / 60));
    return 'GMT' . ($offset ? sprintf('%+03d:%02d', $hours, $minutes) : '');
}

function format_timezone_name($name) {
    $name = str_replace('/', ', ', $name);
    $name = str_replace('_', ' ', $name);
    $name = str_replace('St ', 'St. ', $name);
    return $name;
}

?>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
<select name="timezone" id="timezone">
	<option value="default">-please select-</option>
<?php
foreach( timezone_list() as $k => $v){
	echo '<option value="'.$k.'">'.$v.'</option>';
}
?>
</select>
<input type="submit" value="Submit">
</form>